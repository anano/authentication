package com.example.authentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import android.util.Patterns
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }
    private fun init(){
        auth = Firebase.auth
        RegistrationButton.setOnClickListener {
        signUp()
        }
    }
    private fun signUp() {
        val  email:String = EmailSignUp.text.toString()
        val password:String = PasswordSignUp.text.toString()
        val repeatPassword:String = RepeatPassword.text.toString()
        if (email.isNotEmpty()&& password.isNotEmpty()&& repeatPassword.isNotEmpty()){
            if (password==repeatPassword){
                if (Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                    ProgressBar.visibility = View.VISIBLE
                    auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this) { task ->
                            ProgressBar.visibility = View.GONE
                            if (task.isSuccessful) {
                                // Sign in success, update UI with the signed-in user's information
                                d("signUp", "createUserWithEmail:success")
                                val user = auth.currentUser
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w("signUp", "createUserWithEmail:failure", task.exception)
                                Toast.makeText(baseContext, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show()
                            }

                            // ...
                        }
                    Toast.makeText(this, "signUp is Success!", Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(this, "Email format in not Correct!", Toast.LENGTH_SHORT).show()
                }

            }else {
                Toast.makeText(this,"please type the same passwords", Toast.LENGTH_SHORT).show()
            }
        }else {
            Toast.makeText(this,"please fill all of the fields", Toast.LENGTH_SHORT).show()
        }


    }
}
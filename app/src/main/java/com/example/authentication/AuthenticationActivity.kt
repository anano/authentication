package com.example.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_authentication.*

class AuthenticationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)
        init()
    }
    private fun init(){
        SignUpButton.setOnClickListener {
            val intent: Intent = Intent(this,SignUpActivity::class.java)
            startActivity(intent)


        }
        SignInButton.setOnClickListener {
            val intent: Intent = Intent(this,SignInActivity::class.java)
            startActivity(intent)
        }
    }
}